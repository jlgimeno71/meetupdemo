import pytest
from webdriver_manager.firefox import GeckoDriverManager
from selenium import webdriver
from selenium.webdriver.firefox.service import Service


@pytest.fixture
def browser():
    service = Service(executable_path=GeckoDriverManager().install())
    options = webdriver.FirefoxOptions()
    options.headless = True
    browser = webdriver.Firefox(options=options, service=service)
    yield browser
    browser.quit()
